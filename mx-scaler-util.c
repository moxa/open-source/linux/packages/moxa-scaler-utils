/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <search.h>
#include <dlfcn.h>
#include <json-c/json.h>
#include "moxa-scaler.h"

static int cmd_set_init(void *lib, struct json_object *config, struct scaler_cmd *cmd_set, struct option *options, char *optstr, size_t num){
	int i, param, idx;
	struct array_list *user_cmds;
	struct json_object *cmd_info;
	const char *cmd_name, *opt, *long_opt, *send_func, *recv_func;
	
	if (obj_get_arr(config, "USER_CMDS", &user_cmds) < 0)
		return -1; 
	
	idx=0;
	for (i=0; i<num; i++) {
		if(arr_get_obj(user_cmds, i, &cmd_info) < 0)
			return -1;
		obj_get_str(cmd_info, "CMD_NAME", &cmd_name);
		obj_get_str(cmd_info, "OPT", &opt);
		obj_get_str(cmd_info, "LONG_OPT", &long_opt);
		obj_get_int(cmd_info, "PARAM", &param);
		obj_get_str(cmd_info, "SEND_FUNC", &send_func);
		obj_get_str(cmd_info, "RECV_FUNC", &recv_func);
		
		cmd_set[i] = (struct scaler_cmd){(char *)cmd_name, opt[0], dlsym(lib, send_func), dlsym(lib, recv_func)};	
		options[i] = (struct option){long_opt, param, 0, opt[0]};	
		optstr[idx] = opt[0];
		if(param){
			optstr[idx+1] = ':';
			idx++;
		}
		idx++;
	}
	options[num+1] = (struct option){"help", 0, 0, 'h'};
	optstr[idx] = 'h';
	idx++;
	options[num+2] = (struct option){0, 0, 0, 0};
	
	return idx;
}

int usage(struct json_object *config)
{
	struct array_list *user_cmds;
	struct json_object *cmd_info;
	int num, i;
	const char *help;
	
	if (obj_get_arr(config, "USER_CMDS", &user_cmds) < 0)
		return 0; 
	if (obj_get_int(config, "NUM_OF_USER_CMDS", &num) < 0)
		return 0;
	printf("Usage:\n");
	printf("	mx-scaler-util [Options]...\n");
	printf("Options:\n");
	for (i = 0; i < num; i++){
		if(arr_get_obj(user_cmds, i, &cmd_info) < 0)
			return 0;
		obj_get_str(cmd_info, "HELP", &help);
		printf("%s", help);
	
	}
	printf( "\n");
}

int main(int argc, char *argv[])
{
	int fd, c, ret, optstr_len;
	size_t num;
	void *lib_handle;
	const char *lib_path;
	char *optstr;
	struct json_object *config;
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	struct scaler_cmd *cmd_set;
	struct option *options;
	struct scaler_cmd *cmd;

	config = json_object_from_file(CONF_FILE);
	if (config == NULL){
		printf("No conf file found\n");
		return 1;
	}

	if (obj_get_int(config, "NUM_OF_USER_CMDS", &ret) < 0)
		return 1;
	
	num = ret;

	if (obj_get_str(config, "LIB_PATH", &lib_path) < 0)
		return 1;

	lib_handle = dlopen(lib_path,RTLD_LAZY);
	if (!lib_handle) {
        	fprintf (stderr, "%s\n", dlerror());
        	goto err_dl;
    	}
	
	cmd_set = malloc(num * sizeof(struct scaler_cmd));
	options = malloc((num + 3) * sizeof(struct option));
	optstr = malloc(num * 2 * sizeof(char));

	ret = cmd_set_init(lib_handle, config, cmd_set, options, optstr, num);
        if (ret < 0){
		fprintf (stderr, "%s\n", "init failed");
		goto exit;
	}

	if (argc == 1 || argc > 5) {
		usage(config);
		return 1;
	}

	c = getopt_long(argc, argv, optstr, options, NULL);
	if (c == -1){
		goto exit;
	}
	else if(c == 'h'){
		usage(config);
		goto exit;
	}
		
	memset(send_buf, 0, sizeof(send_buf));
	cmd = get_cmd_by_opt(cmd_set, c, &num);
	if(cmd && cmd->cmd_send_func){
		ret = cmd->cmd_send_func(argv, optind, send_buf);
		if(ret < 0){
			usage(config);
			goto exit;
		}
		else{
			#ifndef LOCAL
			fd = socket_connect(SCALER_SOCKET);
			if (fd < 0)
				goto exit;

			ret = cmd_send(fd, recv_buf, send_buf, ret);
			close(fd);
			#else
			print_cmd(send_buf, ret);
			#endif
		}
	}
	else{
		usage(config);
		goto exit;
	}	

	#ifdef LOCAL
	memset(recv_buf, 0, sizeof(recv_buf));
	memcpy(recv_buf, "\x06\xFF\x42\x5A\x5A\x01\x02\xFF\x00\x00", 9);
	#endif
	cmd = get_cmd_by_name(cmd_set, recv_buf + 2, &num);
	if (cmd){
		#ifdef DEBUG
		print_cmd(recv_buf, ret);
		#endif
		
		cmd->cmd_recv_func(recv_buf);
	}
	else{
		printf("Undefined cmd response: \n");
		print_cmd(recv_buf, ret);
	} 

exit:
	free(cmd_set);
	free(options);
	free(optstr);
err_dl:
	dlclose(lib_handle);
	return 0;
}

