/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <time.h>
#include <errno.h>
#include <termios.h>
#include <pthread.h>
#include "moxa-scaler.h"

struct scalerd {
	int fd;
	int sockfd;
};

pthread_mutex_t scd_mutex;

void sleep_ms( int milliseconds) {

#if _POSIX_C_SOURCE >= 199309L
	struct timespec ts;
	ts.tv_sec = milliseconds / 1000;
	ts.tv_nsec = (milliseconds % 1000) * 1000000;
	nanosleep(&ts, NULL);
#else
	usleep(milliseconds * 1000);
#endif
}

static int set_tty(int fd)
{
	struct termios termio;

	tcgetattr(fd, &termio);

	termio.c_iflag = IGNBRK;
	termio.c_oflag = 0;
	termio.c_lflag = 0;
	termio.c_cflag = B9600 | CS8 | CREAD | CLOCAL;
	termio.c_cflag &= ~CRTSCTS;
	termio.c_iflag &= ~(IXON|IXOFF|IXANY);

	termio.c_cc[VMIN] = 1;
	termio.c_cc[VTIME] = 5;
	termio.c_cflag &= ~(PARENB | PARODD);
	termio.c_cflag &= ~CSTOPB;

	tcsetattr(fd, TCSANOW, &termio);
	tcflush(fd, TCIOFLUSH);
	return fd;
}

static int open_tty(char *ttyname)
{
	int fd;

	fd = open(SCALER_DEVICE, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd < 0) {
		fprintf(stderr, "Open %s: %s\n", ttyname, strerror(errno));
		return -1;
	}

	if (set_tty(fd) < 0) {
		fprintf(stderr, "Set tty failed: %s\n", strerror(errno));
		close(fd);
		return -1;
	}
	return fd;
}

static int scaler_recv(int fd, char *buf, int timeout)
{
	int ret, offset, pkt_len;
	time_t start_time, current_time;

	memset(buf, 0, MAX_PKT_LEN);
	offset = 0;
	pkt_len = MAX_PKT_LEN;
	start_time = time(NULL);

	while (offset < pkt_len) {
		current_time = time(NULL);
		if (current_time - start_time > timeout){
			return -1;
		}

		ret = read(fd, &buf[offset], 1);
		if (ret < 0) {
			if (errno == EAGAIN) {
				if (offset == 0){
					return -2;
				}
				continue;
			} else {
				perror("read");
				return -3;
			}
		}
		if (offset == 5)
			pkt_len = (int) buf[5] + HDR_LEN + 3;
		offset++;
	}

	if (!validate_cmd_checksum(buf,offset))
		return -5;
	return offset;
}

static int read_scaler_ret(int fd, char *buf, int timeout) {
	int ret;
	time_t start_time, current_time;

	start_time = time(NULL);
	while (1) {
		current_time = time(NULL);
		if (current_time - start_time > timeout)
			return -1;

		ret = scaler_recv(fd, buf, TIMEOUT);
		if (ret != EAGAIN)
			return ret;
	}
}

static int exec_cmd (char *ret, char *cmd)
{
	FILE *fp;
	char command[MAX_SH_SIZE];
	memset(command, 0, MAX_SH_SIZE);
	snprintf(command, sizeof(command), cmd);
	fp = popen(command, "r");
	if(NULL == fp) {
		perror("Cannot execute command \n");
		exit(1);
	}
	fgets(ret, MAX_SH_SIZE, fp);
	return pclose(fp);
}

static void *server_thread(void *arg)
{
	struct scalerd *scd = (struct scalerd *) arg;
	int ret, cli_fd, flen;
	char buf[MAX_PKT_LEN];

	while(1){
		// Accept connection from client
		cli_fd = accept(scd->sockfd, NULL, &flen);
		if (cli_fd < 0) {
			fprintf(stderr, "Socket accept failed: %s\n", strerror(errno));
			continue;
		}
	
		// Read from client
		memset(buf, 0, MAX_PKT_LEN);
		ret = read(cli_fd, buf, MAX_PKT_LEN); 
		if (ret < 0) {
			write(cli_fd, "\x15\xFF\x45\x52\x52", 5);
			close(cli_fd);
			continue;
		}
		
		#ifdef DEBUG	
		printf("Get from utility:\n");
		print_cmd(buf, ret);
		#endif

		if (!validate_cmd_checksum(buf, ret)) {
			write(cli_fd, "\x15\xFF\x45\x52\x52", 5);
			close(cli_fd);
			continue;
		} 
		else {
			pthread_mutex_lock(&scd_mutex);
			ret = write(scd->fd, buf, ret);
			if (ret < 0) {
				perror("write");
				pthread_mutex_unlock(&scd_mutex);
				write(cli_fd, "\x15\xFF\x45\x52\x52", 5);
				close(cli_fd);
				continue;
			}
			sleep_ms(100);	
			ret = read_scaler_ret(scd->fd, buf, TIMEOUT);
			if (ret < 0) {
				write(cli_fd, "\x15\xFF\x45\x52\x52", 5);
			} 
			else {
				#ifdef DEBUG
				printf("Receive from scaler:\n");
				print_cmd(buf, ret);
				#endif
				write(cli_fd, buf, ret);				
			}
	
			pthread_mutex_unlock(&scd_mutex);
		}
		close(cli_fd);
		#ifdef DEBUG
		printf("--------------------\n");
		#endif
	}

	pthread_exit(NULL);
}

static int start_daemon(const char *sockname)
{
	int sockfd, socklen;
	struct sockaddr_un sockadr;

	sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sockfd < 0) {
		fprintf(stderr, "Open socket: %s\n", strerror(errno));
		return -1;
	}

	sockadr.sun_family = AF_UNIX;
	strcpy(sockadr.sun_path, sockname);

	unlink(sockname);
	socklen = sizeof(sockadr.sun_family) + strlen(sockadr.sun_path);

	if (bind(sockfd, (struct sockaddr *) &sockadr, socklen) < 0) {
		fprintf(stderr, "Bind socket failed: %s\n", strerror(errno));
		return -1;
	}

	if (listen(sockfd, 1) < 0) {
		fprintf(stderr, "Socket listen failed: %s\n", strerror(errno));
		return -1;
	}

	return sockfd;
}

static void detect_event(struct scalerd *scd)
{
	int ret;
	char buf[MAX_SH_SIZE];

	while (1) {
		pthread_mutex_lock(&scd_mutex);
		ret = read_scaler_ret(scd->fd, buf, TIMEOUT);
		pthread_mutex_unlock(&scd_mutex);
		if (ret >= 0) {
			if (!strncmp("FUN", buf + 2, 3)) {
				printf("Function key pressed\n");
				ret = exec_cmd(buf, FUNC_KEY_SH);
			}
			printf("--------------------\n");
			continue;
		}
		sleep_ms(10);
	}

	return;
}

int main(int argc, char *argv[])
{
	pthread_t socket_tid;
	struct scalerd scd;

	scd.fd = open_tty(SCALER_DEVICE);
	if (scd.fd < 0)
		return 1;

	scd.sockfd = start_daemon(SCALER_SOCKET);
	if (scd.sockfd < 0)
		return 1;

	pthread_create(&socket_tid, NULL, server_thread, (void *) &scd);
	detect_event(&scd);

	close(scd.sockfd);
	close(scd.fd);
	return 0;
}



