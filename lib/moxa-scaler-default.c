/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include "../moxa-scaler.h"

/* CMD format: chk bit(2) + cmd id(3) + data len(1) + hdr checksum(1) + data(data len) + data checksum(1) */
/* checksum = 0xff - (data sum & 0xff) */
#define BUZZER_CTL_HDR		"\x07\xFF\x42\x5A\x5A"
#define ECDIS_MODE_HDR		"\x07\xFF\x45\x4D\x53"
#define ECDIS_QUERY_HDR		"\x07\xFF\x44\x4C\x3F"
#define ECDIS_DOWNLOAD_HDR	"\x07\xFF\x44\x4C\x4E"
#define FWR_VERSION_QUERY_HDR	"\x07\xFF\x53\x57\x49"
#define MODEL_NAME_QUERY_HDR	"\x07\xFF\x54\x59\x50"
#define BRIGHTNESS_CTL_HDR	"\x07\xFF\x42\x4C\x49"
#define TOUCH_BTN_CTL_HDR	"\x07\xFF\x54\x43\x42"

int buzzer_control_send(char *argv[], int optind, unsigned char *send_buf){
	int ret;
	if (!strncmp("on", argv[optind-1], 2)){
		ret = make_packet(send_buf, BUZZER_CTL_HDR, 1, "\xFF");
	}
	else if (!strncmp("off", argv[optind-1], 3)){
		ret = make_packet(send_buf, BUZZER_CTL_HDR, 1, "\x00");
	}
	else {
		return -1;
	}
	return ret;
}

void buzzer_control_recv(unsigned char *recv_buf){
	printf("Set buzzer to %s\n", recv_buf[7] ? "on" : "off");
}

int fwr_version_send(char *argv[], int optind, unsigned char *send_buf){
	int ret;
	ret = make_packet(send_buf, FWR_VERSION_QUERY_HDR, 0, NULL);
	return ret;
}

void fwr_version_recv(unsigned char *recv_buf){
	int plen = recv_buf[5];
	printf("Firmware version: %.*s\n", plen, recv_buf+7);
}

int model_name_send(char *argv[], int optind, unsigned char *send_buf){
	int ret;
	ret = make_packet(send_buf, MODEL_NAME_QUERY_HDR, 0, NULL);
	return ret;
}

void model_name_recv(unsigned char *recv_buf){
	int plen = recv_buf[5];
	printf("Model Name: %.*s\n", plen, recv_buf+7);
}

int brightness_control_send(char *argv[], int optind, unsigned char *send_buf){
	
	int ret, val;
	unsigned char buf;
	if (argv[optind]) {
		val = (int)strtol(argv[optind], NULL, 10);
		if (0 <= val && val <= 255) {
			buf = (unsigned char)val;
			ret = make_packet(send_buf, BRIGHTNESS_CTL_HDR, 1, &buf);
		}
		else{
			return -1;
		}
	}
	else {
			ret = make_packet(send_buf, BRIGHTNESS_CTL_HDR, 0, NULL);
	}
	return ret;
}

void brightness_control_recv(unsigned char *recv_buf){
	printf("Current brightness is %d\n", recv_buf[7]);
}

int ecdis_mode_send(char *argv[], int optind, unsigned char *send_buf){
	int ret, val;
	unsigned char buf;

	if (argv[optind]) {
		val = (int)strtol(argv[optind], NULL, 10);
		if (0 <= val && val <= 2) {
			buf = (unsigned char)val;
			ret = make_packet(send_buf, ECDIS_MODE_HDR, 1, &buf);
		}
		else{
			return -1;
		}
	}
	else {
		ret = make_packet(send_buf, ECDIS_MODE_HDR, 1, "\x3F");
	}
	return ret;
}

void ecdis_mode_recv(unsigned char *recv_buf){
	if(recv_buf[7] == 0)
		printf("Current ecdis mode is DAY \n");
	else if(recv_buf[7] == 1)
		printf("Current ecdis mode is DUSK \n");
	else if(recv_buf[7] == 2)
		printf("Current ecdis mode is NIGHT \n");
	else
		printf("Current ecdis mode is NONE \n");
}

static int get_bit(int num, int idx){
	int bit;
	bit = num >> idx;
	bit &= 0x00000001;
	return bit;
}

int touch_button_ctl_send(char *argv[], int optind, unsigned char *send_buf){
	int ret, val;
	unsigned char data_buf[2];
	if(argv[optind] && argv[optind+1]){
		val = (int)strtol(argv[optind], NULL, 10);
		if (0 <= val && val <= 4) {
			data_buf[0] = val;
			if (!strncmp("on", argv[optind + 1], 2)){
				memcpy(data_buf + 1, "\xFF", 1);
				ret = make_packet(send_buf, TOUCH_BTN_CTL_HDR, 2, data_buf);
			}
			else if (!strncmp("off", argv[optind + 1], 3)){
				memcpy(data_buf + 1, "\x00", 1);
				ret = make_packet(send_buf, TOUCH_BTN_CTL_HDR, 2, data_buf);
			}
			else{
				return -1;
			}	
		}
		else
			return -1;
	}
	else {
		ret = make_packet(send_buf, TOUCH_BTN_CTL_HDR, 1, "\x3F");
	}
	return ret;
}

void touch_button_ctl_recv(unsigned char *recv_buf){
	const char *btn_list[] = {"pwr","br+","br-","func","ecdis"};
	int i;
	if(recv_buf[5] == 2){
		printf("Set btn %s to %s\n", btn_list[recv_buf[7]], recv_buf[8] ? "on" : "off");
	}
	else if(recv_buf[5] == 1){
		for(i=0; i<(sizeof(btn_list)/sizeof(btn_list[0]));i++){
			printf("Btn %s status: %s\n", btn_list[i],get_bit(recv_buf[7],i) ? "on" : "off");
		}
	}
}

int custom_cmd_send(char *argv[], int optind, unsigned char *send_buf){
	int val, idx;
	idx = 0;
	char *token = strtok(argv[optind-1], " ");
	while(token!=NULL){
		val = (int)strtol(token,NULL,16);
		send_buf[idx] = val;
		idx++;
		token = strtok(NULL, " ");
	}
	if (!validate_cmd_checksum(send_buf, idx)){
		printf("Command format error\n");
		return -1;
	}
	return idx;
}
