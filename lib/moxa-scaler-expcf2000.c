/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include "../moxa-scaler.h"

/* CMD format: chk bit(2) + cmd id(3) + data len(1) + hdr checksum(1) + data(data len) + data checksum(1) */
/* checksum = 0xff - (data sum & 0xff) */
#define FWR_VERSION_QUERY_HDR	"\x07\xFF\x53\x57\x49"
#define MODEL_NAME_QUERY_HDR	"\x07\xFF\x54\x59\x50"
#define BRIGHTNESS_CTL_HDR	"\x07\xFF\x42\x4C\x49"
#define TOUCH_BTN_CTL_HDR	"\x07\xFF\x54\x43\x42"
#define LIGHT_SENSOR_CTL_HDR	"\x07\xFF\x4C\x53\x43"
#define LIGHT_SENSOR_LEVEL_HDR	"\x07\xFF\x4C\x4C\x3F"
#define TOUCH_PANEL_CTL_HDR	"\x07\xFF\x50\x54\x43"

int isNumber(char *n) {
	int i = strlen(n);
	int isnum = (i > 0);

	while (i-- && isnum) {
		if (!(n[i] >= '0' && n[i] <= '9')) {
			isnum = 0;
		}
	}

	return isnum;
}

int fwr_version_send(char *argv[], int optind, unsigned char *send_buf) {
	int ret;
	ret = make_packet(send_buf, FWR_VERSION_QUERY_HDR, 0, NULL);
	return ret;
}

void fwr_version_recv(unsigned char *recv_buf) {
	int plen = recv_buf[5];
	printf("Firmware version: %.*s\n", plen, recv_buf+7);
}

int model_name_send(char *argv[], int optind, unsigned char *send_buf) {
	int ret;
	ret = make_packet(send_buf, MODEL_NAME_QUERY_HDR, 0, NULL);
	return ret;
}

void model_name_recv(unsigned char *recv_buf) {
	int plen = recv_buf[5];
	printf("Model Name: %.*s\n", plen, recv_buf+7);
}

int brightness_control_send(char *argv[], int optind, unsigned char *send_buf) {
	int ret, val;
	unsigned char buf;
	if (argv[optind]) {
		if (!isNumber(argv[optind])) {
			printf("\nError: The input value '%s' is invalid.\n\n", argv[optind]);
			return -1;
		}

		val = (int)strtol(argv[optind], NULL, 10);
		if (0 <= val && val <= 10) {
			buf = (unsigned char)val;
			ret = make_packet(send_buf, BRIGHTNESS_CTL_HDR, 1, &buf);
		} else {
			printf("\nError: The input value %d is out of range.\n\n", val);
			return -1;
		}
	} else {
		ret = make_packet(send_buf, BRIGHTNESS_CTL_HDR, 0, NULL);
	}

	return ret;
}

void brightness_control_recv(unsigned char *recv_buf) {
	printf("Current brightness is %d\n", recv_buf[7]);
}

int lightsensor_control_send(char *argv[], int optind, unsigned char *send_buf) {
	int ret, val;
	unsigned char buf;
	if (argv[optind]) {
		if (!isNumber(argv[optind])) {
			printf("\nError: The input value '%s' is invalid.\n\n", argv[optind]);
			return -1;
		}

		val = (int)strtol(argv[optind], NULL, 10);
		if (0 <= val && val <= 2) {
			buf = (unsigned char)val;
			ret = make_packet(send_buf, LIGHT_SENSOR_CTL_HDR, 1, &buf);
		} else {
			printf("\nError: The input value %d is out of range.\n\n", val);
			return -1;
		}
	} else {
		ret = make_packet(send_buf, LIGHT_SENSOR_CTL_HDR, 1, "\x3F");
	}

	return ret;
}

void lightsensor_control_recv(unsigned char *recv_buf) {
	printf("Current light sensor mode is %d\n", recv_buf[7]);
}

int lightsensor_level_send(char *argv[], int optind, unsigned char *send_buf) {
	int ret;
	ret = make_packet(send_buf, LIGHT_SENSOR_LEVEL_HDR, 0, NULL);
	return ret;
}

void lightsensor_level_recv(unsigned char *recv_buf) {
	int plen = recv_buf[5];
	printf("Current Light Sensor Level value: %d\n", recv_buf[7]);
}

int touch_button_ctl_send(char *argv[], int optind, unsigned char *send_buf) {
	int ret, val;
	unsigned char data_buf[2];

	if (argv[optind] && argv[optind+1]) {
		if (!isNumber(argv[optind])) {
			printf("\nError: The input value '%s' is invalid.\n\n", argv[optind]);
			return -1;
		}

		val = (int)strtol(argv[optind], NULL, 10);
		if (0 <= val && val <= 3) {
			data_buf[0] = val;
			if (val == 3) data_buf[0] = '\xFF';
			if (!strncmp("on", argv[optind + 1], 2)) {
				memcpy(data_buf + 1, "\xFF", 1);
				ret = make_packet(send_buf, TOUCH_BTN_CTL_HDR, 2, data_buf);
			} else if (!strncmp("off", argv[optind + 1], 3)) {
				memcpy(data_buf + 1, "\x00", 1);
				ret = make_packet(send_buf, TOUCH_BTN_CTL_HDR, 2, data_buf);
			} else {
				printf("\nError: The input value '%s' is invalid.\n\n", argv[optind+1]);
				return -1;
			}	
		} else {
			printf("\nError: The input value '%s' is invalid.\n\n", argv[optind]);
			return -1;
		}
	} else {
		ret = make_packet(send_buf, TOUCH_BTN_CTL_HDR, 1, "\x3F");
	}

	return ret;
}

void touch_button_ctl_recv(unsigned char *recv_buf) {
	const char *btn_list[] = {"pwr","br+","br-","all"};
	int i;
	int max_index = (sizeof(btn_list)/sizeof(btn_list[0]));

	if(recv_buf[5] == 2) {
		if (recv_buf[7] == 255) {
			printf("Set %s buttons to %s\n", btn_list[3], recv_buf[8] ? "on" : "off");
		} else {
			printf("Set button %s to %s\n", btn_list[recv_buf[7]], recv_buf[8] ? "on" : "off");
		}
	} else if(recv_buf[5] == 1) {
		for(i = 0; i < max_index - 1; i++) {
			printf("Btn %s status: %s\n", btn_list[i], (recv_buf[7] >> max_index - i) & 1 ? "on" : "off");
		}
	}
}

int touch_panel_control_send(char *argv[], int optind, unsigned char *send_buf) {
	int ret, val;
	unsigned char buf;
	if (argv[optind]) {
		if (!isNumber(argv[optind])) {
			printf("\nError: The input value '%s' is invalid.\n\n", argv[optind]);
			return -1;
		}

		val = (int)strtol(argv[optind], NULL, 10);
		if (0 <= val && val <= 1) {
			if (val == 1) {
				buf = '\xFF';
			} 
			buf = (unsigned char)val;
			ret = make_packet(send_buf, TOUCH_PANEL_CTL_HDR, 1, &buf);
		} else {
			printf("\nError: The input value %d is out of range.\n\n", val);
			return -1;
		}
	} else {
		ret = make_packet(send_buf, TOUCH_PANEL_CTL_HDR, 1, "\x3F");
	}

	return ret;
}

void touch_panel_control_recv(unsigned char *recv_buf) {
	printf("Current panel touch control is %d\n", recv_buf[7]);
}

int custom_cmd_send(char *argv[], int optind, unsigned char *send_buf) {
	int val, idx;
	idx = 0;
	char *token = strtok(argv[optind-1], " ");
	while(token!=NULL){
		val = (int)strtol(token,NULL,16);
		send_buf[idx] = val;
		idx++;
		token = strtok(NULL, " ");
	}
	if (!validate_cmd_checksum(send_buf, idx)){
		printf("Command format error\n");
		return -1;
	}
	return idx;
}
