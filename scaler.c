#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <search.h>
#include <json-c/json.h>


#include "moxa-scaler.h"

int socket_connect(const char *sockname)
{
	int fd, len;
	struct sockaddr_un sockadr;

	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0) {
		fprintf(stderr, "Open socket: %s\n", strerror(errno));
		return -1;
	}

	sockadr.sun_family = AF_UNIX;
	strcpy(sockadr.sun_path, sockname);
	len = sizeof(sockadr.sun_family) + strlen(sockadr.sun_path);

	if (connect(fd, (struct sockaddr *) &sockadr, len) < 0) {
		fprintf(stderr, "Connect socket: %s\n", strerror(errno));
		return -1;
	}

	return fd;
}

void print_cmd(unsigned char *buf, int len)
{
	int i;
	for (i=0; i<len; i++)
		printf("0x%02X ", buf[i]);
	printf("\n");
}

int validate_cmd_hdr(unsigned char *cmd){
	unsigned char checksum = 0x0;
	checksum = cmd[0] + cmd[1] + cmd[2] + cmd[3] + cmd[4] + cmd[5];
	checksum = 0xff - (checksum & 0xff);
	if (checksum != cmd[6]){
		return -1;
	}
	return 0;
}

int validate_cmd_checksum(unsigned char *cmd, int data_len){
	unsigned char hdr_chk = 0x0, data_chk = 0x0;
	int i;

	hdr_chk = cmd[0] + cmd[1] + cmd[2] + cmd[3] + cmd[4] + cmd[5];
	hdr_chk = 0xff - (hdr_chk & 0xff);
	if (hdr_chk != cmd[6]){
		return 0;
	}

	if(cmd[5]){
		for (i = DATA_HEAD; i < data_len - 1; i++){
			data_chk += cmd[i];
		}
		data_chk = 0xff - (data_chk & 0xff);
		if (data_chk != cmd[data_len-1]){
			return 0;
		}
	}
	return 1;
}

int make_packet(unsigned char *send_buf, unsigned char *cmd_hdr, int data_len, unsigned char *data){
	unsigned char hdr_checksum = 0x0, data_checksum = 0x0;
	int i;

	for (i=0; i<HDR_LEN; i++)
		hdr_checksum += cmd_hdr[i];
	hdr_checksum = 0xff - ((hdr_checksum + (unsigned char)data_len) & 0xff);
	for (i=0; i< data_len; i++)
		data_checksum += data[i];
	data_checksum = 0xff - (data_checksum & 0xff);

	memcpy(send_buf, cmd_hdr, 5);
	send_buf[HDR_LEN] = data_len;
	send_buf[HDR_LEN+1] = hdr_checksum;
	if ( data != NULL ){
		memcpy(send_buf + DATA_HEAD, data, data_len);
		send_buf[DATA_HEAD + data_len] = data_checksum;
		return DATA_HEAD + data_len + 1;
	}
	return DATA_HEAD;
}

int cmd_send(int fd, unsigned char *read_buf, unsigned char *send_buf, int pkt_len)
{
	int ret;

	ret = write(fd, send_buf, pkt_len);
	if (ret < 0)
		return -1;

	memset(read_buf, 0, MAX_PKT_LEN);
	ret = read(fd, read_buf, MAX_PKT_LEN);
	if (ret < 0)
		return -1;

	return ret;
}


int comp_cmd_name(const void *c1, const void *c2){
	const struct scaler_cmd *cmd1 = c1, *cmd2 = c2;

	return memcmp(cmd1->cmd_name, cmd2->cmd_name, 3);
}

int comp_cmd_opt(const void *c1, const void *c2){
	const struct scaler_cmd *cmd1 = c1, *cmd2 = c2;
	if(cmd1->cmd_opt == cmd2->cmd_opt)
		return 0;
	else
		return -1;
}

struct scaler_cmd *get_cmd_by_name(struct scaler_cmd cmd_set[], char *cmd_name, size_t *num){
	struct scaler_cmd target = { cmd_name, 0, NULL, NULL };

	return lfind(&target, cmd_set, num, sizeof(cmd_set[0]), comp_cmd_name);
}

struct scaler_cmd *get_cmd_by_opt(struct scaler_cmd cmd_set[], int c, size_t *num){
	struct scaler_cmd target = { NULL, c, NULL, NULL };
	return lfind(&target, cmd_set, num, sizeof(cmd_set[0]), comp_cmd_opt);
}

int obj_get_str(struct json_object *config, char *key, const char **buf){
	struct json_object *tmp;

	if (!json_object_object_get_ex(config, key, &tmp))
		return -1;
	*buf = json_object_get_string(tmp);

	return 0;
}

int obj_get_int(struct json_object *config, char *key, int *num){
	struct json_object *tmp;

	if (!json_object_object_get_ex(config, key, &tmp))
		return -1;
	*num = json_object_get_int(tmp);

	return 0;
}

int obj_get_arr(struct json_object *config, char *key, struct array_list **arr){
	struct json_object *tmp;

	if (!json_object_object_get_ex(config, key, &tmp))
		return -1;

	*arr = json_object_get_array(tmp);
	return 0;
}

int arr_get_obj(struct array_list *arr, int idx, struct json_object **val)
{
	if (arr == NULL || idx >= arr->length)
		return -1;

	*val = array_list_get_idx(arr, idx);
	return 0;
}

