# moxa-scaler-utils

**moxa-scaler-utils** provides utility and daemon to control MCU scaler, 
including the light-sensor, brightness, touch-button functions for panel.

## Support Models

- EXPC-F2000

## Usage

- install necessary libary
```
apt update && apt install libjson-c-dev
```

- build code, and install scaler daemon and utility
```
make
make install
```

- start systemd service
```
cp mx-scalerd.service /lib/systemd/system/
systemctl enable mx-scalerd.service

# start and check service status
systemctl start mx-scalerd.service
systemctl status mx-scalerd.service
```

the mx-scalerd service status shall be like:
```
● mx-scalerd.service - Moxa scaler daemon service
     Loaded: loaded (/lib/systemd/system/mx-scalerd.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2022-06-28 02:28:17 EDT; 15min ago
   Main PID: 15525 (mx-scalerd)
      Tasks: 2 (limit: 18923)
     Memory: 208.0K
        CPU: 4.163s
     CGroup: /system.slice/mx-scalerd.service
             └─15525 /usr/sbin/mx-scalerd

Jun 28 02:28:17 debian systemd[1]: Started Moxa scaler daemon service.
```

- scaler utility

For `EXPC-F2000` series config:
```
Usage:
        mx-scaler-util [Options]...
Options:
        -v, --version
                Get scaler firmware version
        -m, --model
                Get model name
        -l, --lightsensor_level
                Get current light sensor level value
        -s, --lightsensor [0~2]
                Set light sensor mode, query without arg
        -b, --brightness [0~10]
                Set brightness, query without arg
        -t, --touch-button [0~3] [on|off]
                Set touch button status status:
                        0: pwr button
                        1: br+ button
                        2: br- button
                        3: all buttons
                        Query all button status without arg
        -p, --touch-panel [0~1]
                Set touch panel on(1)/off(0), query without arg
```
