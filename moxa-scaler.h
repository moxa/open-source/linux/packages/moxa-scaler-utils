/*
 * Copyright (C) MOXA Inc. All rights reserved.
 * Authors:
 *     2022  Elvis Yao <ElvisCW.Yao@moxa.com>
 * This software is distributed under the terms of the MOXA SOFTWARE NOTICE.
 * See the file MOXA-SOFTWARE-NOTICE for details.
 */

#include <json-c/json.h>
#define SCALER_SOCKET "/var/run/moxa_scaler_socket"
#define SCALER_DEVICE "/dev/ttyS2"
#define CONF_FILE "/etc/moxa-configs/moxa-scaler-util.json"
#define FUNC_KEY_SH "/usr/bin/moxa-scaler-funckey-event"

#define HDR_LEN			5
#define DATA_HEAD		7
#define MAX_PKT_LEN		40
#define TIMEOUT			5
#define MAX_SH_SIZE 		128
struct scaler_cmd {
    char *cmd_name;
    int cmd_opt;
    int (*cmd_send_func)(char **, int, unsigned char *);
    void (*cmd_recv_func)(unsigned char *args);
};

int socket_connect(const char *sockname);
void print_cmd(unsigned char *buf, int len);
int validate_cmd_checksum(unsigned char *cmd, int data_len);
int make_packet(unsigned char *send_buf, unsigned char *cmd_hdr, int data_len, unsigned char *data);
int cmd_send(int fd, unsigned char *read_buf, unsigned char *send_buf, int pkt_len);
struct scaler_cmd *get_cmd_by_name(struct scaler_cmd cmd_set[], char *cmd_name, size_t *num);
struct scaler_cmd *get_cmd_by_opt(struct scaler_cmd cmd_set[], int c, size_t *num);
int obj_get_str(struct json_object *config, char *key, const char **buf);
int obj_get_int(struct json_object *config, char *key, int *num);
int obj_get_arr(struct json_object *config, char *key, struct array_list **arr);
int arr_get_obj(struct array_list *arr, int idx, struct json_object **val);

