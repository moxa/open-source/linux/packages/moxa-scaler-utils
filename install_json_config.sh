#!/bin/bash
#
# install_json_config.sh
#
# SPDX-License-Identifier: Apache-2.0
#
# Authors:
#	2022	Elvis Yao <ElvisCW.Yao@moxa.com>
# Description:
#	For installing json config for specific device
#

DMIDECODE="/usr/sbin/dmidecode"

load_product_name_from_dmi() {
	local ret=1

	for m in $($DMIDECODE -t 12 | grep "Option " | awk -F ':' '{print substr($2,1,11)}' | sed 's/ //g');
	do
		if [[ ${PRODUCT_PROFILE[$m]} ]]; then
				${PRODUCT_PROFILE[$m]}
				ret=0
				break
		fi
	done

	return $ret
}

_product_EXPCF2000() {
	TARGET_CONFIG="configs/expc-f2000/moxa-scaler-util.json"
}

_product_MPCM3000() {
	TARGET_CONFIG="configs/mpc-m3000/moxa-scaler-util.json"
}

declare -A PRODUCT_PROFILE=(
	["EXPCF2000"]=_product_EXPCF2000
	["MPCM3000"]=_product_MPCM3000
)

load_product_name_from_dmi

if [ -f "${TARGET_CONFIG}" ]
then
	mkdir -p /etc/moxa-configs
	cp $TARGET_CONFIG /etc/moxa-configs/moxa-scaler-util.json
fi
