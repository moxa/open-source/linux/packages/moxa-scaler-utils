CROSS_COMPILE =
CC := $(CROSS_COMPILE)gcc
STRIP := $(CROSS_COMPILE)strip
CFLAGS:=-Wall -fPIC
LIB_PATH := ./lib
UTIL := mx-scaler-util
DAEMON := mx-scalerd
SRC := $(wildcard $(LIB_PATH)/*.c)
OBJ := $(patsubst $(LIB_PATH)/%.c, %.so, $(SRC))

all: clean $(UTIL)

libscaler.a:  
	gcc $(CFLAGS) -c scaler.c -ljson-c
	ar r libscaler.a scaler.o

$(UTIL): $(UTIL).c libscaler.a $(OBJ)
	$(CC) $(DEBUG) -o $(UTIL) $(UTIL).c -L. -lscaler -ldl -ljson-c
	$(CC) $(DEBUG) -o $(DAEMON) $(DAEMON).c -lpthread -L. -lscaler -ljson-c
	$(STRIP) -s $(UTIL)
	$(STRIP) -s $(DAEMON)

%.so: $(LIB_PATH)/%.c
	$(CC) -fPIC -shared -I $(LIB_PATH) -o $@ $< -L. -lscaler  


debug: DEBUG = -DDEBUG
debug: clean $(UTIL)

local_debug: DEBUG = -DDEBUG -DLOCAL
local_debug: clean $(UTIL)

install:
	/usr/bin/install -d $(DESTDIR)/sbin
	/usr/bin/install $(UTIL) $(DESTDIR)/sbin/$(UTIL)
	/usr/bin/install $(DAEMON) $(DESTDIR)/sbin/$(DAEMON)
	/usr/bin/install *.so $(DESTDIR)/usr/local/lib/
	$(shell ./install_json_config.sh)

.PHONY: clean
clean:
	rm -f $(DAEMON) $(UTIL) *.a *.o *.so

